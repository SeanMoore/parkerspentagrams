use rayon::prelude::*;
use regex::Regex;
use std::collections::{HashMap, HashSet};
use std::fs::File;
use std::fs::OpenOptions;
use std::io::prelude::*;

fn is_pentagram(word: &&str) -> bool {
    word.len() == 5
}

fn has_only_unique_characters(word: &&str) -> bool {
    word.chars().collect::<HashSet<_>>().len() == word.len()
}

fn find_tilings<'a>(
    length: usize,
    visited: Vec<usize>,
    frontier: &HashSet<usize>,
    anagrams: &HashMap<String, Vec<&'a str>>,
    anagram_list: &Vec<&String>,
    skip_sets: &Vec<HashSet<usize>>,
    tilings: &mut Vec<Vec<Vec<&'a str>>>,
) {
    if length <= visited.len() {
        let tiling = visited
            .iter()
            .map(|id| {
                anagrams
                    .get(anagram_list[id.clone()])
                    .unwrap()
                    .iter()
                    .copied()
                    .collect::<Vec<_>>()
            })
            .collect::<Vec<_>>();
        tilings.push(tiling.clone());
    } else {
        for head in frontier {
            let mut visited_head = visited.clone();
            visited_head.push(head.clone());
            let visited_head = visited_head;

            let frontier_head = frontier
                .intersection(&skip_sets[head.clone()])
                .copied()
                .collect::<HashSet<usize, _>>();
            let frontier_head = frontier_head;
            find_tilings(
                length,
                visited_head,
                &frontier_head,
                anagrams,
                anagram_list,
                skip_sets,
                tilings,
            );
        }
    }
}

fn find_tilings_parallel(
    length: usize,
    visited: Vec<usize>,
    frontier: &HashSet<usize>,
    anagrams: &HashMap<String, Vec<&str>>,
    anagram_list: &Vec<&String>,
    skip_sets: &Vec<HashSet<usize>>,
    file: &mut File,
) -> Result<(), std::io::Error> {
    let all_tilings = frontier
        .par_iter()
        .map(|head| {
            let mut visited_head = visited.clone();
            visited_head.push(head.clone());
            let visited_head = visited_head;

            let frontier_head = frontier
                .intersection(&skip_sets[head.clone()])
                .copied()
                .collect::<HashSet<usize, _>>();
            let frontier_head = frontier_head;
            let mut tilings = Vec::new();
            find_tilings(
                length,
                visited_head,
                &frontier_head,
                anagrams,
                anagram_list,
                skip_sets,
                &mut tilings,
            );
            tilings
        })
        .collect::<Vec<_>>();
    for tiling_job in all_tilings {
        for tiling in tiling_job {
            writeln!(file, "{:?}", tiling)?;
        }
    }
    Ok(())
}

fn main() {
    let newline = Regex::new(r"[\r\n]+").unwrap();

    // Read in all words from https://github.com/dwyl/english-words/blob/master/words_alpha.txt
    let words_alpha = std::env::args()
        .nth(1)
        .expect("Expected 'words_alpha.txt' file path as argument.");

    let mut words_alpha_file = File::open(words_alpha).expect("File not found");
    let mut words_alpha_file_string = String::new();
    words_alpha_file
        .read_to_string(&mut words_alpha_file_string)
        .expect("Error while reading file");
    let words_alpha_file_string_trim = words_alpha_file_string.trim();
    let words = newline
        .split(&words_alpha_file_string_trim)
        .collect::<Vec<_>>();

    // Remove any possible duplicates.
    let mut uniques = HashSet::new();
    for word in words.into_iter() {
        uniques.insert(word);
    }
    println!("Unique words: {}", uniques.len());

    // Remove any words that are not made of 5 characters.
    let pentagrams = uniques.into_iter().filter(is_pentagram);

    // Remove any words that reuse their own characters.
    let autouniques = pentagrams
        .filter(has_only_unique_characters)
        .collect::<Vec<_>>();

    // Find all anagram classes.
    let mut anagrams = HashMap::<String, Vec<&str>>::new();
    for word in autouniques.into_iter() {
        let mut anagram_unsorted = word.chars().collect::<Vec<_>>();
        anagram_unsorted.sort();
        let anagram = anagram_unsorted.into_iter().collect::<String>();

        match anagrams.get_mut(&anagram) {
            Some(anagram_class) => {
                anagram_class.push(word);
            }
            None => {
                anagrams.insert(anagram, vec![word]);
            }
        }
    }
    let anagrams = anagrams;
    println!("Anagram classes: {}", anagrams.len());

    // Give each anagram an ID.
    let mut anagram_list = anagrams.keys().collect::<Vec<_>>();
    anagram_list.sort();
    let anagram_list = anagram_list;
    let anagram_sets = anagram_list
        .iter()
        .map(|anagram| anagram.chars().collect::<HashSet<_>>())
        .collect::<Vec<_>>();

    // Generate the upper triangle of an adjacency matrix.
    let mut skip_sets = Vec::new();
    for (i, anagram_a) in anagram_sets.iter().enumerate() {
        let mut skip_set = HashSet::new();
        for (j, anagram_b) in anagram_sets.iter().enumerate().skip(i) {
            if anagram_a.is_disjoint(anagram_b) {
                skip_set.insert(j);
            } else {
                // println!("{:?} {:?}", anagram_a, anagram_b);
            }
        }
        skip_sets.push(skip_set);
    }
    let skip_sets = skip_sets;

    let mut tilings_file = OpenOptions::new()
        .create(true)
        .write(true)
        .truncate(true)
        .open("tilings.txt")
        .expect("Unable to open file for writing.");

    find_tilings_parallel(
        5,
        vec![],
        &(0..anagram_sets.len()).collect::<HashSet<usize>>(),
        &anagrams,
        &anagram_list,
        &skip_sets,
        &mut tilings_file,
    )
    .unwrap();
}
