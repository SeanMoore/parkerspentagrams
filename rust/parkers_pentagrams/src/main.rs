use regex::Regex;
use std::collections::HashMap;
use std::fs::File;
use std::fs::OpenOptions;
use std::io::prelude::*;

#[derive(Debug)]
enum Error {
    UnknownCharacter,
    NotPentagram,
    NonUniqueCharacters,
}

type Result<T> = std::result::Result<T, Error>;

const COMPLETION_LENGTH: usize = 5;
type Index = u16; // This is a bit faster over usize within large memories.

#[derive(PartialEq, Eq, Hash, Copy, Clone, Ord, PartialOrd)]
struct Anagram {
    vec: u32,
}

impl Anagram {
    fn empty() -> Anagram {
        Self { vec: 0 }
    }
    fn new(text: &str) -> Result<Anagram> {
        if text.len() != 5 {
            return Err(Error::NotPentagram);
        }

        Self::new_relaxed_count(text)
    }
    fn new_relaxed_count(text: &str) -> Result<Anagram> {
        let mut vec: u32 = 0;
        for c in text.chars() {
            match c {
                'a'..='z' => {
                    let bvec = 1 << ((c as u32) - ('a' as u32));
                    if 0 != (vec & bvec) {
                        return Err(Error::NonUniqueCharacters);
                    } else {
                        vec |= bvec;
                    }
                }
                _ => return Err(Error::UnknownCharacter),
            }
        }
        Ok(Self { vec })
    }
    fn is_disjoint(&self, rhs: &Anagram) -> bool {
        0 == (self.vec & rhs.vec)
    }
    fn join(&self, rhs: &Anagram) -> Anagram {
        Self {
            vec: self.vec | rhs.vec,
        }
    }
    fn overlap_count(&self, rhs: &Anagram) -> u32 {
        (self.vec & rhs.vec).count_ones()
    }
    fn letters_count(&self) -> u32 {
        self.vec.count_ones()
    }
}

struct VisitChain<'a> {
    anagram: Anagram,
    parent: Option<&'a VisitChain<'a>>,
}

impl<'a> VisitChain<'a> {
    fn iter(&'a self) -> VisitChainIterator<'a> {
        VisitChainIterator {
            cursor: Some(&self),
        }
    }
}

struct VisitChainIterator<'a> {
    cursor: Option<&'a VisitChain<'a>>,
}

impl<'a> Iterator for VisitChainIterator<'a> {
    type Item = Anagram;
    fn next(&mut self) -> Option<Self::Item> {
        match self.cursor {
            Some(v) => {
                self.cursor = v.parent;
                Some(v.anagram)
            }
            None => None,
        }
    }
}

fn find_tilings(
    pseudovowels: &Vec<Pseudovowel>,
    anagrams: &HashMap<Anagram, Vec<&str>>,
    anagram_list: &Vec<&Anagram>,
    compatible_skip: &Vec<Vec<Index>>,
    selection: Index,
    visit_depth: usize,
    visited: Option<&VisitChain>,
    visit_jumble: Anagram,
    // checks: &mut usize,
    file: &mut File,
) -> std::result::Result<(), std::io::Error> {
    if COMPLETION_LENGTH == visit_depth {
        let visit_chain = visited.unwrap();
        let tiling = visit_chain
            .iter()
            .map(|anagram| anagrams.get(&anagram).unwrap())
            .collect::<Vec<_>>();
        writeln!(file, "{:?}", tiling)?;
        // println!("{:?}", tiling);
    } else {
        for pseudovowel in pseudovowels {
            if pseudovowel.width
                < visit_jumble.overlap_count(&pseudovowel.anagram)
                    + ((COMPLETION_LENGTH - visit_depth) as u32)
            {
                return Ok(()); // We don't have enough letters in the pseudovowel list to finish.
            }
        }

        for head in compatible_skip[selection as usize].iter() {
            // *checks += 1;
            let anagram = anagram_list[*head as usize];
            if anagram.is_disjoint(&visit_jumble) {
                let visit_chain = VisitChain {
                    anagram: anagram.clone(),
                    parent: visited,
                };

                // let tiling = visited_head
                //     .iter()
                //     .map(|anagram| match anagrams.get(&anagram) {
                //         Some(x) => x.clone(),
                //         None => vec![],
                //     })
                //     .collect::<Vec<_>>();
                // println!("{:?}", tiling);

                find_tilings(
                    pseudovowels,
                    anagrams,
                    anagram_list,
                    compatible_skip,
                    *head,
                    visit_depth + 1,
                    Some(&visit_chain),
                    visit_jumble.join(&anagram),
                    // checks,
                    file,
                )?;
            }
        }
    }
    Ok(())
}

struct CompatibilityCount {
    index: Index,
    compatibilities: usize,
}

struct Pseudovowel {
    anagram: Anagram,
    width: u32,
}

impl Pseudovowel {
    fn new(text: &str) -> Self {
        let anagram = Anagram::new_relaxed_count(text).unwrap();
        Self {
            anagram,
            width: anagram.letters_count(),
        }
    }
}

fn main() {
    let newline = Regex::new(r"[\r\n]+").unwrap();

    // This could be considered cheating.
    let pseudovowels = vec![
        Pseudovowel::new("aeiouwyd"),
        Pseudovowel::new("aeilnorsu"),
        Pseudovowel::new("heiantolu"),
    ];

    // Read in all words from https://github.com/dwyl/english-words/blob/master/words_alpha.txt
    let words_alpha = std::env::args()
        .nth(1)
        .expect("Expected 'words_alpha.txt' file path as argument.");

    let mut words_alpha_file = File::open(words_alpha).expect("File not found");
    let mut words_alpha_file_string = String::new();
    words_alpha_file
        .read_to_string(&mut words_alpha_file_string)
        .expect("Error while reading file");
    let words_alpha_file_string_trim = words_alpha_file_string.trim();

    // Find all anagram classes.
    let mut anagrams = HashMap::<Anagram, Vec<&str>>::new();
    for word in newline.split(&words_alpha_file_string_trim) {
        match Anagram::new(word) {
            Ok(anagram) => match anagrams.get_mut(&anagram) {
                Some(anagram_class) => {
                    anagram_class.push(word);
                }
                None => {
                    anagrams.insert(anagram, vec![word]);
                }
            },
            Err(_) => {}
        }
    }
    let anagrams = anagrams;
    println!("Anagram classes: {}", anagrams.len());

    // Give each anagram an ID.
    let mut anagram_list = anagrams.keys().collect::<Vec<_>>();
    anagram_list.sort(); // For debugging purposes sort it for stable indexing.
    let anagram_list = anagram_list;

    // Create a pairwise skip table so that we know which checks can be skipped entirely.
    // Note that this has to be a full adjacency matrix for later steps and not just the upper triangle.
    let mut compatible_count = (0..anagram_list.len())
        .map(|i| CompatibilityCount {
            index: i as Index,
            compatibilities: 0,
        })
        .collect::<Vec<_>>();
    for (a, anagram_a) in anagram_list.iter().enumerate() {
        for (_, anagram_b) in anagram_list.iter().enumerate() {
            if anagram_a.is_disjoint(&anagram_b) {
                compatible_count[a].compatibilities += 1;
            }
        }
    }

    // Reorder the indexed list of compatibilities by the number of compatibilities.
    compatible_count.sort_by(|a, b| {
        match a.compatibilities.partial_cmp(&b.compatibilities).unwrap() {
            std::cmp::Ordering::Equal => a.index.partial_cmp(&b.index).unwrap(),
            x => x,
        }
    });
    let compatible_count = compatible_count;

    // Reorder the original anagram list to put the anagrams with the fewest compatibilities first.
    let anagram_list_reorder = (0..anagram_list.len())
        .map(|i| anagram_list[compatible_count[i].index as usize])
        .collect::<Vec<_>>();

    // Create a shrink-wrapped 2D compatibility table with indices over reordered anagrams.
    let mut compatible_skip = vec![Vec::new(); anagram_list_reorder.len() + 1]; // The last row is the "no words selected line."
    for i in 0..anagram_list_reorder.len() {
        let anagram_a = anagram_list_reorder[i];
        let row = &mut compatible_skip[i];
        for j in 0..anagram_list_reorder.len() {
            let anagram_b = anagram_list_reorder[j];
            if i < j && (anagram_a.is_disjoint(anagram_b)) {
                row.push(j as Index);
            }
        }
    }
    compatible_skip[anagram_list_reorder.len()] =
        (0..anagram_list_reorder.len() as Index).collect();
    let compatible_skip = compatible_skip;

    // for anagram in anagram_list_reorder.iter() {
    //     println!("anagram={:?}", anagrams.get(anagram).unwrap());
    // }
    // for compat in compatible_count.iter() {
    //     println!("index={0} count={1}", compat.index, compat.compatibilities);
    // }
    // for anagram in anagram_list_reorder.iter() {
    //     println!("anagram={0:#028b}", anagram.vec);
    // }
    // for (i, line) in compatible_skip.iter().enumerate() {
    //     print!("{0}: ", i);
    //     for item in line {
    //         print!("{0} ", item);
    //     }
    //     println!("");
    // }
    // for pseudovowel in &pseudovowels {
    //     for anagram in &anagram_list_reorder {
    //         if anagram.is_disjoint(&pseudovowel.anagram) {
    //             println!(
    //                 "anagram={0:#028b} is disjoint with pseudovowel={1:#028b}",
    //                 anagram.vec, pseudovowel.anagram.vec
    //             );
    //         }
    //     }
    // }

    let mut tilings_file = OpenOptions::new()
        .create(true)
        .write(true)
        .truncate(true)
        .open("tilings.txt")
        .expect("Unable to open file for writing.");

    // let mut checks = 0;
    find_tilings(
        &pseudovowels,
        &anagrams,
        &anagram_list_reorder,
        &compatible_skip,
        anagram_list_reorder.len() as Index,
        0,
        None,
        Anagram::empty(),
        // &mut checks,
        &mut tilings_file,
    )
    .unwrap();
    // println!("checks={0}", checks);
}
