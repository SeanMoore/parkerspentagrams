#!/usr/bin/env python3


# Can you find: five five-letter words with twenty-five unique letters?
# Matt Parker's version ran in 32 days and showcased a solution that took 15 minutes.
# This implementation runs in about 3.5 minutes.
# https://www.youtube.com/watch?v=_-AfhLQfb6w&t=1s


def is_pentagram(word):
    return len(word) == 5


def has_only_unique_characters(word):
    return len(set(word)) == len(word)


def merge(dict1, dict2):
    z = dict1.copy()   # start with keys and values of x
    z.update(dict2)    # modifies z with keys and values of y
    return z


# Read in all words from https://github.com/dwyl/english-words/blob/master/words_alpha.txt
words = []
with open('words_alpha.txt', 'r') as f:
    words = f.read().splitlines()
print("Words:", len(words))

# Remove any possible duplicates.
uniques = list(set(words))
print("Unique words:", len(uniques))

# Remove any words that are not made of 5 characters.
pentagrams = filter(is_pentagram, uniques)

# Remove any words that reuse their own characters.
autouniques = filter(has_only_unique_characters, pentagrams)

# Find all anagram classes.
anagrams = {}
for w in autouniques:
    anagram = ''.join(sorted(list(w)))
    if anagram in anagrams:
        anagrams[anagram].append(w)
    else:
        anagrams[anagram] = [w]
print("Anagram classes:", len(anagrams))

# Give each anagram an ID.
anagram_list = sorted(list(anagrams.keys()))
anagram_sets = []
for v in anagram_list:
    anagram_sets.append(set(v))

# Generate the upper triangle of an adjacency matrix.
skip_sets = []
for i, anagramA in enumerate(anagram_sets):
    skip_set = set()
    for j in range(i+1, len(anagram_sets)):
        anagramB = anagram_sets[j]
        if anagramA.isdisjoint(anagramB):
            skip_set.add(j)
    skip_sets.append(skip_set)


def from_anagram(id):
    return anagrams[anagram_list[id]]


with open('tilings.txt', 'w') as f:
    def find_tilings(length, visited, frontier):
        if length <= len(visited):
            tiling = list(map(from_anagram, visited))
            print(tiling, file=f)
        for head in frontier:
            find_tilings(length, visited+[head], frontier & skip_sets[head])

    find_tilings(5, [], set(range(len(anagram_sets))))
