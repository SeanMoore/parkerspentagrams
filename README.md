# Parker's Pentagrams

## Motivation

This code base is a response to [Matt Parker's YouTube video](https://www.youtube.com/watch?v=_-AfhLQfb6w). In it he asks "[H]ow many five-letter words can you find where together they don't have any letters repeated?".

[![Can you find: five five-letter words with twenty-five unique letters?
](https://img.youtube.com/vi/_-AfhLQfb6w/0.jpg)](https://www.youtube.com/watch?v=_-AfhLQfb6w)

[The code Matt wrote](https://github.com/standupmaths/fiveletterworda) is admittedly not well optimized taking approximately 32 days to run. And there is another solution to the problem that was showcased that took 15 minutes complete. This seemed great but while Matt was able to explain his algorithm in the video the quicker one wasn't explained. So this code base exists as a self-contained code example which should run in about 3.5 minutes in the single-threaded version and about 40 seconds for the multi-process version on 8 CPUs.

##### Okay, but why the name?

Because in language statistics an combination of a number of words is called an "n-gram" and often given a Latin numerical prefix e.g. unigram, bigram, trigram and so on. The largest answers to Matt Parker's question contain 5, 5 letter words and hence "pentagram".

## Prerequisites

- [The primary word list in Matt Parker's video.](https://github.com/dwyl/english-words/blob/master/words_alpha.txt)
- [Python 3](https://www.python.org/downloads/)


## Operation

- De-duplicate any possible repeated words.
- Filter out words that don't contain exactly 5 letters.
- Filter out words that contain repeated letters.
- Create a dictionary where the keys are a canonical representation of an anagram of each word so that words that are an anagram of each other will be added as a value of the dictionary.
- Place those canonical anagrams in a sorted list so they have de facto IDs.
- Generate the upper triangle of a sparse adjacency "matrix" where each row of the matrix is a set of IDs of other anagrams that row's anagram is compatible with that has a higher ID than that row's anagram.
  - This can be conceptualized as a dense, boolean, adjacency matrix where the lower right triangle is cut off and the elements of the matrix which are true have their column number row-aggregated into a set. Where a disjunction occurs for the set of IDs a boolean AND would occur for the conceptual dense matrix.
- Pentagram tilings are then found by calling `find_tilings` which iterates over each anagram which then iterates over every anagram in its `frontier`, recursively calling `find_tilings` but with the selected anagram concatenated to the `visited` list describing how the recursive call arrived there and passing a disjunction of the `frontier` and the selected anagram's adjacencies. When the `visited` list is 5 elements long it gets printed to a file.
  - Why this works: As a graph, all these adjacencies are bidirectional so only the upper triangle of its adjacency matrix is necessary. Any subset of 5 anagrams that satisfy the query must each be compatible with each other and so must form a 5-clique. One way you could identify a 5-clique in a  dense boolean matrix (with both upper and lower triangles) would be to bitwise AND the 5 rows and still have their corresponding columns still be logically true. But the way to do it in a sparse matrix with a recursive descent search and the lower triangle missing would be to pick the first element and make sure that the next element is reachable by all the previous elements and that `frontier` that it keeps doing a disjunction on is how that is tracked.